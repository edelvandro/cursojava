/*
 * Leia a hora inicial e a hora final de um jogo. A seguir calcule a duração do jogo, sabendo que o mesmo pode
 * começar em um dia e terminar em outro, tendo uma duração mínima de 1 hora e máxima de 24 horas.
 */

package listas;

import java.util.Scanner;

public class exer04Lista02 {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int inicio = sc.nextInt();
        int fim = sc.nextInt();

        int duracao = (fim - inicio);

        if (fim == inicio) {
            System.out.println("O jogo durou 24 horas");
        } else if (fim > inicio) {
            duracao = fim - inicio;
            System.out.printf("O jogo durou %d horas", duracao);
        } else {
            duracao = (24 - inicio) + fim;
            System.out.printf("O jogo durou %d horas", duracao);
        }

        sc.close();
    }

}
