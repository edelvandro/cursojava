/*
 * Leia um valor inteiro N. Este valor será a quantidade de valores inteiros X que serão lidos em seguida.
 * Mostre quantos destes valores X estão dentro do intervalo [10,20] e quantos estão fora do intervalo, mostrando
 * essas informações conforme exemplo (use a palavra "in" para dentro do intervalo, e "out" para fora do intervalo).
 */


package listas;

import java.util.Scanner;

public class exer02Lista04 {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);


        int in = 0, out = 0, num = 0;

        System.out.print("Digite uma valor: ");
        int valor = sc.nextInt();

        for (int i = 0; i < valor; i++) {
            System.out.print("Digite um novo número:");
            num = sc.nextInt();

            if ((num >= 10) && (num <= 20)) {
                in += 1;
            } else
                out += 1;

        }
        System.out.println();
        System.out.println("in = "+ in);
        System.out.println("out = "+ out);

    }

}
