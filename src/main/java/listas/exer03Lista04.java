package listas;

import java.util.Scanner;

public class exer03Lista04 {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("Digite um valor: ");
        int n = sc.nextInt();

        System.out.print("Digite o primeiro valor: ");
        double valor1 = sc.nextDouble();

        System.out.print("Digite o segundo valor: ");
        double valor2 = sc.nextDouble();

        System.out.print("Digite o terceiro valor: ");
        double valor3 = sc.nextDouble();

        double media = (valor1 * 2.0 + valor2 * 3.0 + valor3 * 5.0) / 10;

        System.out.printf("\nMédia = %.1f\n", media);

        sc.close();

    }

}
