/*
 * Fazer um progrmama para ler as medidas dos lados de dois triângulos X e Y (suponha medidas válidas).
 * Em seguida, mostrar o valor das áreas dos dois triângulos
 * e dizer qual dos dois triângulos possui a maior área.
 *
 * area = Math.sqrt(p(p-a)(p-b)(p-c))  onde p = a + b + c /2
 */

package listas;

import java.util.Scanner;

public class exer09Lista04 {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        double xA, xB, xC, yA, yB, yC;

        System.out.println("Entre com os lados do tringulo x: ");
        xA = sc.nextDouble();
        xB = sc.nextDouble();
        xC = sc.nextDouble();

        System.out.println("Entre com os lados do tringulo y: ");
        yA = sc.nextDouble();
        yB = sc.nextDouble();
        yC = sc.nextDouble();

        double p = (xA + xB + xC) / 2.0;
        double areaX = Math.sqrt(p * (p - xA) * (p - xB) * (p - xC));

        p = (yA + yB + yC) / 2.0;
        double areaY = Math.sqrt(p * (p - yA) * (p - yB) * (p - yC));

        System.out.printf("A área do triângulo X: %.4f\n", areaX);
        System.out.printf("A área do triângulo Y: %.4f\n", areaY);

        if (areaX > areaY) {
            System.out.println("Maior área eá a do triangulo X ");

        } else {
            System.out.prinln("Maior área triangulo Y ");
        }

        sc.close();

    }

}

