/*
* Leia 2 valores inteiros (A e B). Após, o programa deve mostrar uma mensagem "Sao Multiplos" ou "Nao sao Multiplos",
* indicando se os valores lidos são múltiplos entre si. Atenção: os números devem poder ser digitados em
* ordem crescente ou decrescente.
*/

package listas;

import java.util.Scanner;

public class exer03Lista02 {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int valor1 = sc.nextInt();
        int valor2 = sc.nextInt();


        if((valor1 % valor2 == 0) || (valor2 % valor1 == 0)){
            System.out.println("São multiplos");
        }else{
            System.out.println("Não São multiplos");
        }

        sc.close();
    }

}
