package listas;

public class exer08Lista04 {

    public static void main(String[] args) {

        String original = "abcde FGHIJ ABC abc DEFG   ";

        String s01 = original.toLowerCase();        // Passa todas as Strings para maiusculas
        String s02 = original.toUpperCase();        // Passa todas para minusculas
        String s03 = original.trim();               // Elimina todoa os espaços antes e/ou depois da String
        String s04 = original.substring(2);         // Imprime do caracter no indice 2 em diante
        String s05 = original.substring(2, 9);      // Imprime do caracter no indice 2 até o 9
        String s06 = original.replace('a', 'x');    // Modifica o caracte indicado pele segundo argumento
        String s07 = original.replace("abc", "xy"); // Modifica o caracte indicado pele segundo argumento
        int i = original.indexOf("bc");             // Primeira ocorrencia do "bc" -1-
        int j = original.lastIndexOf("bc");         // Ultima ocorrencia do "bc" -17-


        System.out.println("Original: -" + original + "-");
        System.out.println("touperCase: -" + s01 + "-");
        System.out.println("toLowerCase: -" + s02 + "-");
        System.out.println("trim: -" + s03 + "-");
        System.out.println("substring: -" + s04 + "-");
        System.out.println("substring: -" + s05 + "-");
        System.out.println("replace ('a', 'x'): -" + s06 + "-");
        System.out.println("replace (abc', 'xy'): -" + s07 + "-");
        System.out.println("indexOf ('bc'): -" + i + "-");
        System.out.println("lastIndexOf ('bc'): -" + j + "-");


        // Split

        String s = "potato apple lemon orange";

        String[] vect = s.split(" ");      // declara um vetor String[] com nome de variavel "vect",
        // recebendo a variavel "s" fazendo o split e usando "espaço" como separador.

        System.out.println(vect[0]);
        System.out.println(vect[1]);
        System.out.println(vect[2]);
        System.out.println(vect[3]);
    }

}
