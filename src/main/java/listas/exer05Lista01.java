/*
 * Fazer um programa para ler o código de uma peça 1, o número de peças 1, o valor unitário de cada peça 1,
 * o código de uma peça 2, o número de peças 2 e o valor unitário de cada peça 2.
 * Calcule e mostre o valor a ser pago.
 */

package listas;

import java.util.Scanner;

public class exer05Lista01 {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int cod_peca1 = sc.nextInt();
        int qtd_peca1 = sc.nextInt();
        double val_peca1 = sc.nextDouble();
        int cod_peca2 = sc.nextInt();
        int qtd_peca2 = sc.nextInt();
        double val_peca2 = sc.nextDouble();

        double valorTotal = ((qtd_peca1 * val_peca1) + (qtd_peca2 * val_peca2));

        System.out.printf("valor total a pagar: R$ %.2f\n", valorTotal);

        sc.close();
    }

}
