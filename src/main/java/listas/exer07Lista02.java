/*
 * Leia 2 valores com uma casa decimal (x e y), que devem representar as coordenadas de um ponto em um plano.
 * A seguir, determine qual o quadrante ao qual pertence o ponto, ou se está sobre um dos eixos cartesianos
 * ou na origem (x = y = 0).
 * Se o ponto estiver na origem, escreva a mensagem “Origem”.
 * Se o ponto estiver sobre um dos eixos escreva “Eixo X” ou “Eixo Y”, conforme for a situação.
 *
 *       Q2 | Q1
 *       ---|---
 *       Q3 | Q4
 */

package listas;

import java.util.Scanner;

public class exer07Lista02 {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.println("Digite o valor X: ");
        double valorX = sc.nextDouble();
        System.out.println("Digite o valor Y: ");
        double valorY = sc.nextDouble();

        if ((valorX == 0) && (valorY == 0)) {
            System.out.println("Origem");

        } else if ((valorX > 0) &&(valorY > 0)) {
            System.out.println("Q1");

        }else if ((valorX < 0) &&(valorY > 0)) {
            System.out.println("Q2");

        }else if ((valorX < 0) &&(valorY < 0)) {
            System.out.println("Q3");
        }else
        System.out.println("Q4");

        sc.close();
    }

}
