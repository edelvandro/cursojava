/*
 * Escreva um programa que repita a leitura de uma senha até que ela seja válida. Para cada leitura de senha
 * incorreta informada, escrever a mensagem "Senha Invalida". Quando a senha for informada corretamente deve ser
 * impressa a mensagem "Acesso Permitido" e o algoritmo encerrado. Considere que a senha correta é o valor 2002.
 * */

package listas;

import java.util.Scanner;

public class exer01Lista03 {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int senha = 2002;

        System.out.println("Digite sua senha: ");
        int numDigitado = sc.nextInt();

        while (senha != numDigitado) {
            System.out.println("Senha inválida");
            System.out.println("Digite sua senha: ");
            numDigitado = sc.nextInt();
        }

        System.out.println("Acesso Permitido");

        sc.close();
    }
}
