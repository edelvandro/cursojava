/*
 * Com base na tabela abaixo, escreva um programa que leia o código de um item e a quantidade deste item.
 * A seguir, calcule e mostre o valor da conta a pagar.
 *
 * cod       especificação           preço
 * 1         cachorro quente         4,00
 * 2         X-salada                4,50
 * 3         X-bacon                 5,00
 * 4         Torrada simples         2,00
 * 5         Refrigerante            1,50
 */


package listas;

import java.util.Scanner;

public class exer05Lista02 {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.println("Digite sua escolha: ");
        int cod = sc.nextInt();
        System.out.println("Digite a quantidade: ");
        int qtd = sc.nextInt();

        String escolha;
        double valor = 0;
        double total = 0;


        switch (cod) {
            case 1:
                escolha = "Cachorro Quente";
                valor = 4.00;
                total = qtd * valor;
                System.out.printf("Você escolheu %d %s e o valor total é de R$ %.2f\n", qtd, escolha, total);
                break;
            case 2:
                escolha = "X-salada";
                valor = 4.50;
                total = qtd * valor;
                System.out.printf("Você escolheu %d %s e o valor total é de R$ %.2f\n", qtd, escolha, total);
                break;
            case 3:
                escolha = "X-bacon";
                valor = 5.00;
                total = qtd * valor;
                System.out.printf("Você escolheu %d %s e o valor total é de R$ %.2f\n", qtd, escolha, total);
                break;
            case 4:
                escolha = "Torrada Simples";
                valor = 2.00;
                total = qtd * valor;
                System.out.printf("Você escolheu %d %s e o valor total é de R$ %.2f\n", qtd, escolha, total);
                break;
            case 5:
                escolha = "Refrigerante";
                valor = 1.50;
                total = qtd * valor;
                System.out.printf("Você escolheu %d %s e o valor total é de R$ %.2f\n", qtd, escolha, total);
                break;
            default:
                System.out.println("Valor invalido!");
        }


        sc.close();

    }

}
