package listas;

/*
* Faça um programa para ler o valor do raio de um círculo,
* e depois mostrar o valor da área deste círculo com quatro casas decimais.
*/

import java.util.Scanner;

public class exer02Lista01 {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        double raio = sc.nextDouble();
        // double area = 3.14159 * (raio * raio);
        // double area = Math.PI * (raio * raio);
        double area = Math.PI * Math.pow(raio, raio);

        System.out.printf("O valor da area desse circulo é: %.4f\n", area);

    }

}
