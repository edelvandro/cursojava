/*
 * Fazer um programa para ler um número N.
 * Depois leia N pares de números e mostre a divisão do primeiro pelo segundo.
 * Se o denominador for igual a zero, mostrar a mensagem "divisao impossivel".
 */

package listas;

import java.util.Scanner;

public class exer04Lista04 {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("Digite um número: ");
        int numero = sc.nextInt();

        for (int i = 0; i < numero; i++) {
            System.out.print("Digite o primeiro valor: ");
            double valor1 = sc.nextInt();
            System.out.print("Digite o segundo valor: ");
            double valor2 = sc.nextInt();

            if (valor2 == 0) {
                System.out.println("Divisão impossível");
            } else {
                System.out.println(valor1 / valor2);
            }
            System.out.println();
        }

        sc.close();

    }

}
