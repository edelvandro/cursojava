/*
 * Fazer um programa para ler um número inteiro e dizer se este número é par ou ímpar.
 */

package listas;

import java.util.Scanner;

public class exer02Lista02 {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int numero = sc.nextInt();

        if (numero % 2 == 0) {
            System.out.printf("O número %d é Par.", numero);
        } else {
            System.out.printf("O número %d é Impar.", numero);
        }

        sc.close();
    }

}
