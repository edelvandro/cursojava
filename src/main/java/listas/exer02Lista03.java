/*
 * Escreva um programa para ler as coordenadas (X,Y) de uma quantidade indeterminada de pontos no sistema cartesiano.
 * Para cada ponto escrever o quadrante a que ele pertence. O algoritmo será encerrado quando pelo menos uma de duas
 * coordenadas for NULA (nesta situação sem escrever mensagem alguma).
 */

package listas;

import java.util.Scanner;

public class exer02Lista03 {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);


        System.out.println("Digite o ponto X: ");
        double x = sc.nextDouble();
        System.out.println("Digite o ponto Y: ");
        double y = sc.nextDouble();

        while ((x != 0) && (y != 0)) {

            if ((x > 0) && (y > 0)) {
                System.out.println("Primeiro");

            } else if ((x > 0) && (y < 0)) {
                System.out.println("Quarto");

            } else if ((x < 0) && (y > 0)) {
                System.out.println("Segundo");

            } else
                System.out.println("Terceiro");

            System.out.println("Digite o ponto X: ");
            x = sc.nextDouble();
            System.out.println("Digite o ponto Y: ");
            y = sc.nextDouble();
        }

        sc.close();

    }

}

