/*
* Fazer um programa que leia o número de um funcionário, seu número de horas trabalhadas,
* o valor que recebe por hora e calcula o salário desse funcionário.
* A seguir, mostre o número e o salário do funcionário, com duas casas decimais.
*/

package listas;

import java.util.Scanner;

public class exer04Lista01 {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int numero_funcionario = sc.nextInt();
        double horas_trabalhadas = sc.nextDouble();
        double valor_hora = sc.nextDouble();

        double salario_funcionario = horas_trabalhadas * valor_hora;

        System.out.printf("O funcionario Nº %d tem o salário no valor de R$ %.2f.", numero_funcionario, salario_funcionario);

        sc.close();

    }

}
