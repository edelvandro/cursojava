/*
* Fazer um programa para ler as medidas da largura e comprimento de um terreno retangular
* com uma casa decimal, bem como o valor do metro quadrado do terreno com duas casas decimais.
* Em seguida, o programa deve mostrar o valor da área do terreno, bem como o valor do preço
* do terreno, ambos com duas casas decimais.
*/

import java.util.Scanner;

public class exer02Aula01 {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        double largura = sc.nextDouble();
        double comprimento = sc.nextDouble();
        double valorM2 = sc.nextDouble();

        double area = largura * comprimento;
        double total = area * valorM2;

        System.out.printf("A área do terreno é: %.2f\n", area );
        System.out.printf("O valor do terreno é: %.2f\n", total );

        sc.close();
    }
}
